//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RV.WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwUser
    {
        public long UserID { get; set; }
        public string UserLogin { get; set; }
        public string UserPassword { get; set; }
        public string UserPasswordAES { get; set; }
        public string UserName { get; set; }
        public long RFPrinterID { get; set; }
        public Nullable<System.DateTime> LastLoginDate { get; set; }
        public Nullable<long> GroupID { get; set; }
        public string PrinterName { get; set; }
        public string PrinterAddress { get; set; }
        public string GroupName { get; set; }
    }
}
