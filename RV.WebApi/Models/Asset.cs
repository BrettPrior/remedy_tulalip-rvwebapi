//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RV.WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Asset
    {
        public long AssetID { get; set; }
        public long AssetTypeID { get; set; }
        public Nullable<long> ItemID { get; set; }
        public string AssetRFID { get; set; }
        public string Attribute1 { get; set; }
        public string Attribute2 { get; set; }
        public string Attribute3 { get; set; }
        public string Attribute4 { get; set; }
        public string Attribute5 { get; set; }
        public string Attribute6 { get; set; }
        public string Attribute7 { get; set; }
        public string Attribute8 { get; set; }
        public string Attribute9 { get; set; }
        public string Attribute10 { get; set; }
        public string Destination { get; set; }
        public Nullable<long> AssetImageID { get; set; }
        public Nullable<long> LocationID { get; set; }
        public Nullable<long> ParentAssetID { get; set; }
        public long LabelID { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public Nullable<System.DateTime> LastMoveDate { get; set; }
        public Nullable<System.DateTime> LastRecvDate { get; set; }
        public Nullable<System.DateTime> LastShipDate { get; set; }
        public Nullable<System.DateTime> LastCountDate { get; set; }
        public Nullable<System.DateTime> LastCustom1Date { get; set; }
        public Nullable<System.DateTime> LastCustom2Date { get; set; }
        public Nullable<System.DateTime> LastCustom3Date { get; set; }
        public Nullable<System.DateTime> LastCustom4Date { get; set; }
        public Nullable<System.DateTime> LastCustom5Date { get; set; }
        public bool Deleted { get; set; }
    }
}
