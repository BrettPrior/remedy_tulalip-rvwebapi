//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RV.WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwAssetImage_MHistory
    {
        public long AssetImage_MHistoryID { get; set; }
        public Nullable<System.DateTime> HistoryDateTime { get; set; }
        public Nullable<long> HistoryTypeID { get; set; }
        public string HistoryType { get; set; }
        public string HistoryBy { get; set; }
        public byte[] Old_AssetImage { get; set; }
        public string Old_ImageName { get; set; }
        public byte[] New_AssetImage { get; set; }
        public string New_ImageName { get; set; }
    }
}
