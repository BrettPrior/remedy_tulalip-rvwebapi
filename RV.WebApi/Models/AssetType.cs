//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RV.WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AssetType
    {
        public long AssetTypeID { get; set; }
        public byte MasterTypeID { get; set; }
        public string AssetType1 { get; set; }
        public string Attribute1 { get; set; }
        public string Attribute2 { get; set; }
        public string Attribute3 { get; set; }
        public string Attribute4 { get; set; }
        public string Attribute5 { get; set; }
        public string Attribute6 { get; set; }
        public string Attribute7 { get; set; }
        public string Attribute8 { get; set; }
        public string Attribute9 { get; set; }
        public string Attribute10 { get; set; }
        public string AssetColor { get; set; }
        public Nullable<long> AssetImageID { get; set; }
        public bool Deleted { get; set; }
    }
}
