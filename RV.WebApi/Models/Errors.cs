﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RV.WebApi.Models
{
    public static class Errors
    {
        public static Dictionary<ErrorNumbers, string> Error;

        static Errors()
        {
            Error = new Dictionary<ErrorNumbers, string>
            {
                { ErrorNumbers.UnhandledException,          "" },
                { ErrorNumbers.CommissionInvalidAssetID,    "Invalid AssetID" },
                { ErrorNumbers.CommissionInvalidUserID,     "Invalid UserID" },
                { ErrorNumbers.MoveBatchInvalidLocation,    "Location not found" },
                { ErrorNumbers.MoveInvalidLocation,         "Location not found" },
                { ErrorNumbers.UpdateUserInvalidAction,     "A valid Action must be passed!  (Add, Delete or Update)" },
                { ErrorNumbers.UpdateUserAddUserIDNotZero,  "UserID needs to be 0 or NULL for Add" },
                { ErrorNumbers.UpdateUserAddUserLoginNull,  "User Login cannot be NULL for Add" },
                { ErrorNumbers.UpdateUserAddUserPassNull,   "User Password cannot be NULL for Add" },
                { ErrorNumbers.UpdateUserDelUserIDNotZero,  "UserID needs to be 0 or NULL for Delete" },
                { ErrorNumbers.UpdateUserDelUserIDNotFound, "UserID not found for Delete" },
                { ErrorNumbers.UpdateUserUpdUserIDNotFound, "UserID not found for Update" },
                { ErrorNumbers.UpdateUserUpdUserLoginNull,  "User Login cannot be NULL for Update" },
                { ErrorNumbers.UpdateUserUpdGroupIDNull,    "GroupID cannot be NULL or 0 for Update" }
            };
        }

        public enum ErrorNumbers
        {
            UnhandledException          = 990001,
            CommissionInvalidAssetID    = 020001,
            CommissionInvalidUserID     = 020002,
            CountInvalidAssetID         = 020101,
            MoveBatchInvalidLocation    = 020201,
            MoveInvalidLocation         = 020301,
            UpdateUserInvalidAction     = 020401,
            UpdateUserAddUserIDNotZero  = 020402,
            UpdateUserAddUserLoginNull  = 020403,
            UpdateUserAddUserPassNull   = 020404,
            UpdateUserDelUserIDNotZero  = 020405,
            UpdateUserDelUserIDNotFound = 020406,
            UpdateUserUpdUserIDNotFound = 020407,
            UpdateUserUpdUserLoginNull  = 020408,
            UpdateUserUpdGroupIDNull    = 020409
        }

    }
}