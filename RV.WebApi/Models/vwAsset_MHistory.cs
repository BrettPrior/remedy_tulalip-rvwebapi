//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RV.WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwAsset_MHistory
    {
        public long Asset_MHistoryID { get; set; }
        public Nullable<System.DateTime> HistoryDateTime { get; set; }
        public string HistoryType { get; set; }
        public Nullable<long> HistoryTypeID { get; set; }
        public string HistoryBy { get; set; }
        public string Old_AssetType { get; set; }
        public string Old_AssetRFID { get; set; }
        public string Old_Attribute1 { get; set; }
        public string Old_Attribute2 { get; set; }
        public string Old_Attribute3 { get; set; }
        public string Old_Attribute4 { get; set; }
        public string Old_Attribute5 { get; set; }
        public string Old_Attribute6 { get; set; }
        public string Old_Attribute7 { get; set; }
        public string Old_Attribute8 { get; set; }
        public string Old_Attribute9 { get; set; }
        public string Old_Attribute10 { get; set; }
        public Nullable<long> Old_AssetImageID { get; set; }
        public string Old_ItemNumber { get; set; }
        public string New_AssetType { get; set; }
        public string New_AssetRFID { get; set; }
        public string New_Attribute1 { get; set; }
        public string New_Attribute2 { get; set; }
        public string New_Attribute3 { get; set; }
        public string New_Attribute4 { get; set; }
        public string New_Attribute5 { get; set; }
        public string New_Attribute6 { get; set; }
        public string New_Attribute7 { get; set; }
        public string New_Attribute8 { get; set; }
        public string New_Attribute9 { get; set; }
        public string New_Attribute10 { get; set; }
        public Nullable<long> New_AssetImageID { get; set; }
        public string New_ItemNumber { get; set; }
    }
}
