﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RV.WebApi.Helpers;
using RV.WebApi.Models;

namespace RV.WebApi.Controllers
{
    public class UpdateUserController : ApiController
    {
        /// <summary>
        /// Pass User object to update. All fields in UpdateUser need to be filled in.  Any field left NULL will set that field to NULL in the database.
        /// </summary>
        public class UpdUserRequest
        {
            public Actions? Action { get; set; }
            public long? UserID { get; set; }
            public string UserLogin { get; set; }
            public string UserName { get; set; }
            public string UserPassword { get; set; }
            public long GroupID { get; set; }
            public long UpdatingUserID { get; set; }
        }

        public class UpdUserResponse
        {
            public UpdUserResponse(bool success) { Success = success; }
            public UpdUserResponse(bool success, string errorMessage) { Success = success; ErrorMessage = errorMessage; }
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
        }

        public enum Actions { Add, Delete, Update }

        // POST api/updateuser
        public UpdUserResponse Post([FromBody]UpdUserRequest request)
        {
            if (request.Action == null) { return new UpdUserResponse(false, "A valid Action must be passed!  (Add, Delete or Update)"); }
            var context = new RFID_RVEntities();

            switch (request.Action)
            {
                case Actions.Add:
                    try
                    {
                        if (request.UserID != null && request.UserID > 0)
                        {
                            return new UpdUserResponse(false, "UserID needs to be 0 or NULL for Add");
                        }
                        if (request.UserLogin == null)
                        {
                            return new UpdUserResponse(false, "UserLogin cannot be NULL");
                        }
                        if (request.UserPassword == null)
                        {
                            return new UpdUserResponse(false, "UserPassword cannot be NULL");
                        }

                        var CryptAES = new EncryptionAES();
                        var passAES = CryptAES.EncryptAES(request.UserPassword);
                        var Crypt64 = new Encryption64();
                        var pass64 = Crypt64.Encrypt64(request.UserPassword);

                        context.spUpdateUser(0, request.UserLogin, pass64, passAES, request.UserName, 0, request.GroupID, request.UpdatingUserID);

                        return new UpdUserResponse(true);
                    }
                    catch (Exception e) { return new UpdUserResponse(false, e.Message); }

                case Actions.Delete:
                    if (request.UserID == null || request.UserID == 0)
                    {
                        return new UpdUserResponse(false, "UserID cannot be NULL or 0");
                    }

                    var user = context.Users
                        .Where(l => l.UserID == request.UserID)
                        .SingleOrDefault();

                    if (user == null || user.UserID != request.UserID) { return new UpdUserResponse(false, $"UserID {request.UserID} not found"); }

                    user.Deleted = true;
                    context.SaveChanges();

                    return new UpdUserResponse(true);

                case Actions.Update:
                    try
                    {
                        if (request.UserID == null || request.UserID == 0)
                        {
                            return new UpdUserResponse(false, "UserID cannot be NULL or 0");
                        }
                        if (request.UserLogin == null)
                        {
                            return new UpdUserResponse(false, "UserLogin cannot be NULL");
                        }
                        if (request.UserPassword == null)
                        {
                            return new UpdUserResponse(false, "UserPassword cannot be NULL");
                        }

                        var CryptAES = new EncryptionAES();
                        var passAES = CryptAES.EncryptAES(request.UserPassword);
                        var Crypt64 = new Encryption64();
                        var pass64 = Crypt64.Encrypt64(request.UserPassword);

                        context.spUpdateUser(request.UserID, request.UserLogin, pass64, passAES, request.UserName, 0, request.GroupID, request.UpdatingUserID);

                        return new UpdUserResponse(true);
                    }
                    catch (Exception e) { return new UpdUserResponse(false, e.Message); }
            }

            return new UpdUserResponse(false, "Action not set");
        }
    }
}