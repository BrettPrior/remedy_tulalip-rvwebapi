﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers.Posts
{
    public class UpdateConfigController : ApiController
    {
        /// <summary>
        /// All fields in UpdateConfig need to be filled in.
        /// </summary>
        public class UpdConfigRequest
        {
            public Actions? Action { get; set; }
            public string SettingName { get; set; }
            public string SettingValue { get; set; }
        }

        public class UpdConfigResponse
        {
            public UpdConfigResponse(bool success) { Success = success; }
            public UpdConfigResponse(bool success, string errorMessage) { Success = success; ErrorMessage = errorMessage; }
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
        }

        public enum Actions { Add, Delete, Update }

        // POST api/updateconfig
        public UpdConfigResponse Post([FromBody]UpdConfigRequest request)
        {
            if (request.Action == null) { return new UpdConfigResponse(false, "A valid Action must be passed!  (Add, Delete or Update)"); }
            var context = new RFID_RVEntities();

            if (request.SettingName == null)
            {
                return new UpdConfigResponse(false, "[Setting Name] cannot be NULL");
            }

            var cfg = context.Configs.Where(c => c.SettingName == request.SettingName).SingleOrDefault();

            switch (request.Action)
            {
                case Actions.Add:
                    try
                    {
                        if (cfg != null) { return new UpdConfigResponse(false, $"Add error!  [Setting Name] <{request.SettingName}> already exists."); }

                        context.Configs.Add(new Config() { SettingName = request.SettingName, SettingValue = request.SettingValue });
                        context.SaveChanges();
                        break;
                    }
                    catch (Exception e) { return new UpdConfigResponse(false, e.Message); }

                case Actions.Delete:
                    try
                    {
                        if (cfg == null) { return new UpdConfigResponse(false, $"Delete error!  [Setting Name] <{request.SettingName}> not found."); }

                    context.Configs.Remove(cfg);
                    context.SaveChanges();
                    break;
                    }
                    catch (Exception e) { return new UpdConfigResponse(false, e.Message); }

                case Actions.Update:
                    try
                    {
                        if (cfg == null) { return new UpdConfigResponse(false, $"Update error!  [Setting Name] <{request.SettingName}> not found."); }

                        cfg.SettingValue = request.SettingValue;
                        context.SaveChanges();
                        break;
                    }
                    catch (Exception e) { return new UpdConfigResponse(false, e.Message); }
            }

            return new UpdConfigResponse(true);
        }
    }
}