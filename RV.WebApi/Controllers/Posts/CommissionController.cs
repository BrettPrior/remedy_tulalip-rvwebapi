﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RV.WebApi.Helpers;
using RV.WebApi.Models;

namespace RV.WebApi.Controllers
{
    public class CommissionController : ApiController
    {
        public class CommissionRequest
        {
            public Asset Asset { get; set; }
            public long LocationID { get; set; }
            public long UserID { get; set; }
        }

        public class CommissionResponse
        {
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
            public int ErrorNumber { get; set; }
        }

        // POST api/Commission
        public CommissionResponse Post([FromBody]CommissionRequest request)
        {
            // encrypt the password that was provided by the user

            var context = new RFID_RVEntities();

            if (request.Asset.AssetID != 0)
                if (context.Assets.Where(a => a.AssetID == request.Asset.AssetID).Count() == 0)
                { return new CommissionResponse { Success = false, ErrorMessage = Errors.Error[Errors.ErrorNumbers.CommissionInvalidAssetID], ErrorNumber = (int)Errors.ErrorNumbers.CommissionInvalidAssetID }; }

            if (request.UserID == 0 || context.Users.Where(u => u.UserID == request.UserID).Count() == 0)
            { return new CommissionResponse { Success = false, ErrorMessage = Errors.Error[Errors.ErrorNumbers.CommissionInvalidUserID], ErrorNumber = (int)Errors.ErrorNumbers.CommissionInvalidUserID }; }

            //Run HHCommission SP
            context.spHHCommission( request.Asset.AssetID, 
                                    request.Asset.AssetTypeID, 
                                    request.Asset.AssetRFID, 
                                    request.Asset.ItemID, 
                                    request.Asset.Attribute1,
                                    request.Asset.Attribute2,
                                    request.Asset.Attribute3,
                                    request.Asset.Attribute4,
                                    request.Asset.Attribute5,
                                    request.Asset.Attribute6,
                                    request.Asset.Attribute7,
                                    request.Asset.Attribute8,
                                    request.Asset.Attribute9,
                                    request.Asset.Attribute10,
                                    request.Asset.AssetImageID,
                                    request.LocationID, 
                                    request.UserID);

            // return the response object
            return new CommissionResponse { Success = true };
        }
    }
}