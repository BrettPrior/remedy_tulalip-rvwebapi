﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RV.WebApi.Helpers;
using RV.WebApi.Models;

namespace RV.WebApi.Controllers
{
    public class MoveBatchController : ApiController
    {
        /// <summary>
        /// Pass AssetRFID and Location info.  If the ID fields are blank then the string field is used.
        /// </summary>
        public class MoveBatchRequest
        {
            public List<MoveAsset> Assets { get; set; }
            public long? LocationID { get; set; }
            public string Location { get; set; }
            public long UserID { get; set; }
        }

        public class MoveBatchResponse
        {
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
            public int ErrorNumber { get; set; }
        }

        public class MoveAsset
        {
            public long? AssetID { get; set; }
            public string AssetRFID { get; set; }
        }

        // POST api/movebatch
        /// <summary>
        /// Move for a Batch of Assets
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public MoveBatchResponse PostBatch([FromBody]MoveBatchRequest request)
        {
            var context = new RFID_RVEntities();

            //Only lookup up LocationID if it is already blank
            if (request.LocationID == null || request.LocationID == 0)
            {
                // query the database for the user
                var location = context.Locations
                    .Where(x => !x.Deleted)
                    .Where(x => x.LocationName == request.Location)
                    .SingleOrDefault();

                // return error if location not found
                if (location == null)
                {
                    return new MoveBatchResponse() { Success = false, ErrorMessage = $"{Errors.Error[Errors.ErrorNumbers.MoveBatchInvalidLocation]} [{request.Location}]", ErrorNumber = (int)Errors.ErrorNumbers.MoveBatchInvalidLocation };
                }
                else
                    request.LocationID = location.LocationID; // Set the ID
            }

            foreach (MoveAsset asset in request.Assets)
            {
                //Run Move SP
                if (asset.AssetID == null)
                    context.spAssetMove(asset.AssetRFID, request.LocationID, "HH", request.UserID, DateTime.Now);
                else
                    context.spAssetMoveByIDs(asset.AssetID, request.LocationID, "HH", request.UserID, DateTime.Now);
            }

            // return the response object
            return new MoveBatchResponse { Success = true };
        }

    }
}