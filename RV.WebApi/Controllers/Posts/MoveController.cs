﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RV.WebApi.Helpers;
using RV.WebApi.Models;

namespace RV.WebApi.Controllers
{
    public class MoveController : ApiController
    {
        /// <summary>
        /// Pass AssetRFID and Location info.  If the ID fields are blank then the string field is used.
        /// </summary>
        public class MoveRequest
        {
            public long? AssetID { get; set; }
            public string AssetRFID { get; set; }
            public long? LocationID { get; set; }
            public string Location { get; set; }
            public long UserID { get; set; }
        }

        public class MoveResponse
        {
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
            public int ErrorNumber { get; set; }
        }

        // POST api/move
        public MoveResponse Post([FromBody]MoveRequest request)
        {
            var context = new RFID_RVEntities();

            //Only lookup up LocationID if it is already blank
            if (request.LocationID == null || request.LocationID == 0)
            {
                // query the database for the user
                var location = context.Locations
                    .Where(x => !x.Deleted)
                    .Where(x => x.LocationName == request.Location)
                    .SingleOrDefault();

                // return error if location not found
                if (location == null)
                {
                    return new MoveResponse() { Success = false, ErrorMessage = $"{Errors.Error[Errors.ErrorNumbers.MoveBatchInvalidLocation]} [{request.Location}]", ErrorNumber = (int)Errors.ErrorNumbers.MoveBatchInvalidLocation };
                }
                else
                    request.LocationID = location.LocationID; // Set the ID
            }

            //Run Move SP
            if (request.AssetID == null)
                context.spAssetMove(request.AssetRFID, request.LocationID, "HH", request.UserID, DateTime.Now);
            else
                context.spAssetMoveByIDs(request.AssetID, request.LocationID, "HH", request.UserID, DateTime.Now);

            // return the response object
            return new MoveResponse { Success = true };
        }
    }
}