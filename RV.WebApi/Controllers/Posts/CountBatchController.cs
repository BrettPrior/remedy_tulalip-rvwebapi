﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RV.WebApi.Helpers;
using RV.WebApi.Models;

namespace RV.WebApi.Controllers
{
    public class CountBatchController : ApiController
    {
        /// <summary>
        /// Pass AssetRFID and Location info.  If the ID fields are blank then the string field is used.
        /// </summary>
        public class CountBatchRequest
        {
            public List<CountAsset> Assets { get; set; }
            public long PhysicalID { get; set; }
            public long PhysicalLocationID { get; set; }
            public DateTime CountDate { get; set; }
            public long UserID { get; set; }
        }

        public class CountAsset
        {
            public long? AssetID { get; set; }
            public string AssetRFID { get; set; }
        }

        public class CountBatchResponse
        {
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
            public int ErrorNumber { get; set; }
        }

        // POST api/login
        /// <summary>
        /// Move for a Batch of Assets
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public CountBatchResponse PostBatch([FromBody]CountBatchRequest request)
        {
            var context = new RFID_RVEntities();

            foreach (CountAsset countAsset in request.Assets)
            {
                //Only lookup up AssetID if it is already blank
                if (countAsset.AssetID == null || countAsset.AssetID == 0)
                {
                    // query the database for the Asset
                    var asset = context.Assets
                        .Where(x => !x.Deleted)
                        .Where(x => x.AssetRFID == countAsset.AssetRFID)
                        .SingleOrDefault();

                    // Bacause this is a batch, we will skip the record if the asset is not found
                    //if (asset == null)
                    //    return new PostResponse() { Success = false, ErrorMessage = $"Count <{countAsset.AssetRFID}> not found" };
                    //else
                    if (asset == null) { countAsset.AssetID = null; } else { countAsset.AssetID = asset.AssetID; }// Set the ID
                }

                //Run Count SP
                if (countAsset.AssetID != null)
                    context.spHHPhysical(request.PhysicalID, request.PhysicalLocationID, countAsset.AssetID, request.CountDate.ToString("G"), request.UserID);
            }

            // return the response object
            return new CountBatchResponse { Success = true };
        }

    }
}