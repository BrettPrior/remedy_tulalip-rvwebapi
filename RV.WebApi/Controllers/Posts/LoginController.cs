﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RV.WebApi.Helpers;
using RV.WebApi.Models;

namespace RV.WebApi.Controllers
{
    public class LoginController : ApiController
    {
        public class LoginRequest
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }

        public class LoginResponse
        {
            public long UserID { get; set; }
            public string UserName { get; set; }

            public List<UserPermission> Permissions { get; set; }
            public List<UserPrompt> Prompts { get; set; }
            public List<Config> Configs { get; set; }
        }

        public class UserPrompt
        {
            public string PromptLabel { get; set; }
            public string PromptValue { get; set; }
        }

        public class UserPermission
        {
            public string PermissionName { get; set; }
            public string PermissionControl { get; set; }
        }

        // POST api/login
        public LoginResponse Post([FromBody]LoginRequest request)
        {
            // encrypt the password that was provided by the user
            var helper = new EncryptionAES();
            var encryptedPassword = helper.EncryptAES(request.Password);

            var context = new RFID_RVEntities();

            // query the database for the user
            var user = context.Users
                .Where(x => !x.Deleted)
                .Where(x => x.UserLogin == request.UserName)
                .Where(x => x.UserPasswordAES == encryptedPassword)
                .SingleOrDefault();

            // return null if we didn't find the user
            if (user == null)
            {
                return null;
            }

            // select groups
            var permissions = context.vwGroups
                .Where(x => x.GroupID == user.GroupID)
                //.Where(x => x.PermissionName.StartsWith("Handheld"))
                .Select(x => new UserPermission { PermissionName = x.PermissionName, PermissionControl = x.PermissionControl })
                .ToList();

            // Get Handheld Customer Permissions
            var customerPermission = context.Configs
                .Where(c => c.SettingName == "Handheld.Customer.Permission")
                .SingleOrDefault();

            if (customerPermission != null)
                permissions.Add(new UserPermission() { PermissionControl = customerPermission.SettingValue, PermissionName = customerPermission.SettingValue });

            // select prompts
            var prompts = context.Prompts
                .Select(x => new UserPrompt { PromptValue = x.PromptValue, PromptLabel = x.PromptLabel })
                .ToList();

            // select configs
            var configs = context.Configs.ToList();

            //Run User Login SP
            context.spUserLogin(user.UserID);

            // return the response object
            return new LoginResponse { UserID = user.UserID, UserName = user.UserName, Permissions = permissions, Prompts = prompts, Configs = configs };
        }
    }
}