﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RV.WebApi.Helpers;
using RV.WebApi.Models;

namespace RV.WebApi.Controllers
{
    public class CountController : ApiController
    {
        /// <summary>
        /// Pass AssetRFID and Location info.  If the ID fields are blank then the string field is used.
        /// </summary>
        public class PostRequest
        {
            public long? AssetID { get; set; }
            public string AssetRFID { get; set; }
            public long PhysicalID { get; set; }
            public long PhysicalLocationID { get; set; }
            public DateTime CountDate { get; set; }
            public long UserID { get; set; }
        }

        public class PostResponse
        {
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
            public int ErrorNumber { get; set; }
        }

        // POST api/count
        public PostResponse Post([FromBody]PostRequest request)
        {
            var context = new RFID_RVEntities();

            //Only lookup up AssetID if it is already blank
            if (request.AssetID == null || request.AssetID == 0)
            {
                // query the database for the Asset
                var asset = context.Assets
                    .Where(x => !x.Deleted)
                    .Where(x => x.AssetRFID == request.AssetRFID)
                    .SingleOrDefault();

                // return error if Asset not found
                if (asset == null)
                    return new PostResponse() { Success = false, ErrorMessage = Errors.Error[Errors.ErrorNumbers.CountInvalidAssetID], ErrorNumber = (int)Errors.ErrorNumbers.CountInvalidAssetID };
                else
                    request.AssetID = asset.AssetID; // Set the ID
            }

            //Run Count SP
            context.spHHPhysical(request.PhysicalID, request.PhysicalLocationID, request.AssetID, request.CountDate.ToString("G"), request.UserID);

            // return the response object
            return new PostResponse { Success = true };
        }
    }
}