﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers.Customer
{
    public class RFIDCustomerHandOffController : ApiController
    {
        /// <summary>
        /// Pass AssetRFIDs to Hand off to Customer
        /// </summary>
        public class RFIDCustomerHandOffRequest
        {
            public List<string> TagIDs { get; set; }
        }

        public class RFIDCustomerHandOffResponse
        {
            public RFIDCustomerHandOffResponse(bool success) { Success = success; }
            public RFIDCustomerHandOffResponse(bool success, string errorMessage) { Success = success; ErrorMessage = errorMessage; }
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
        }

        // POST api/RFIDPick
        public RFIDCustomerHandOffResponse Post([FromBody]RFIDCustomerHandOffRequest request)
        {
            if (request.TagIDs == null || request.TagIDs.Count == 0)
            {
                return new RFIDCustomerHandOffResponse(false, "TagIDs not passed");
            }

            var context = new RFID_RVEntities();

            foreach (string tagID in request.TagIDs)
            {
                context.spCust_Retire(tagID);
            }

            return new RFIDCustomerHandOffResponse(true);
        }
    }
}