﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RV.WebApi.Helpers;
using RV.WebApi.Models;

namespace RV.WebApi.Controllers
{
    public class RFIDReadsController : ApiController
    {
        /// <summary>
        /// Pass Action and Location
        /// </summary>
        public class RFIDReadsRequest
        {
            public Actions? Action { get; set; }
            public long? LocationID { get; set; }
            public string Location { get; set; }
        }

        public class RFIDReadsResponse
        {
            public RFIDReadsResponse(bool success) { Success = success; }
            public RFIDReadsResponse(bool success, string errorMessage) { Success = success; ErrorMessage = errorMessage; }
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
            public object ReturnObject { get; set; }
        }

        private class CurrentReads
        {
            public string Location { get; set; }
            public List<string> TagIDs { get; set; }
        }

        public enum Actions { Start, Stop, Read }

        // POST api/RFIDReads
        public RFIDReadsResponse Post([FromBody]RFIDReadsRequest request)
        {
            if (request.Action == null) { return new RFIDReadsResponse(false, "A valid Action must be passed!  (Start, Stop)"); }

            if ((request.LocationID == null || request.LocationID == 0) && (request.Location == null || request.Location == ""))
            {
                return new RFIDReadsResponse(false, "LocationID or Location must be entered");
            }

            var context = new RFID_RVEntities();

            // Lookup Location based on either LocationID or where Location.Location or Location.ListnerLocation = request.Location
            var loc = context.Locations.Where(l => l.Deleted == false && (l.LocationName == request.Location || l.LocationID == request.LocationID)).SingleOrDefault();
            if (loc == null && (request.LocationID != null && request.LocationID != 0 )) { return new RFIDReadsResponse(false, $"Invalid LocationID <{request.LocationID}>"); }
            if (loc == null && (request.Location != null && request.Location != "" )) { return new RFIDReadsResponse(false, $"Invalid Location <{request.Location}>"); }

            switch (request.Action)
            {
                case Actions.Read:
                    try
                    {
                        CurrentReads currentReads = new CurrentReads() { Location = loc.LocationName, TagIDs = new List<string>() };

                        var Currents = context.CurrentReads.Where(c => c.LocationID == loc.LocationID);

                        if (Currents != null)
                        {
                            foreach (CurrentRead currentRead in Currents)
                                currentReads.TagIDs.Add(currentRead.AssetRFID);
                        }

                        return new RFIDReadsResponse(true) { ReturnObject = currentReads };
                    }
                    catch (Exception e) { return new RFIDReadsResponse(false, e.Message); }

                case Actions.Start:
                    try
                    {
                        var Currents = context.CurrentReads.Where(c => c.LocationID == loc.LocationID);

                        if (Currents != null) { context.CurrentReads.RemoveRange(Currents); context.SaveChanges(); }


                        return new RFIDReadsResponse(true);
                    }
                    catch (Exception e) { return new RFIDReadsResponse(false, e.Message); }

                case Actions.Stop:
                    try
                    {
                        CurrentReads currentReads = new CurrentReads() { Location = loc.LocationName, TagIDs = new List<string>() };

                        var Currents = context.CurrentReads.Where(c => c.LocationID == loc.LocationID);

                        if (Currents != null)
                        {
                            foreach (CurrentRead currentRead in Currents)
                                currentReads.TagIDs.Add(currentRead.AssetRFID);
                        }

                        return new RFIDReadsResponse(true) { ReturnObject = currentReads };
                    }
                    catch (Exception e) { return new RFIDReadsResponse(false, e.Message); }
            }

            return new RFIDReadsResponse(false, "Action not set");
        }
    }
}