﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers.Customer
{
    public class RFIDPickController : ApiController
    {
        /// <summary>
        /// Pass AssetRFIDs to Pick
        /// </summary>
        public class RFIDPickRequest
        {
            public List<string> TagIDs { get; set; }
        }

        public class RFIDPickResponse
        {
            public RFIDPickResponse(bool success) { Success = success; }
            public RFIDPickResponse(bool success, string errorMessage) { Success = success; ErrorMessage = errorMessage; }
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
        }

        // POST api/RFIDPick
        public RFIDPickResponse Post([FromBody]RFIDPickRequest request)
        {
            if (request.TagIDs == null || request.TagIDs.Count == 0)
            {
                return new RFIDPickResponse(false, "TagIDs not passed");
            }

            var context = new RFID_RVEntities();

            // Lookup Location based on Config.DefaultLocation.Pick
            var location = context.Locations
                .Join(context.Configs, loc => loc.LocationName, cfg => cfg.SettingValue, (loc, cfg) => new { Locations = loc, Configs = cfg })
                .Where(l => l.Locations.Deleted == false)
                .Where(c => c.Configs.SettingName == "DefaultLocation.Pick")
                .Select(l => l.Locations).SingleOrDefault();

            if (location == null) { return new RFIDPickResponse(false, $"Default Pick Location not configured"); }

            foreach (string tagID in request.TagIDs)
            {
                context.spCust_Pick(tagID, location.LocationID);
            }

            return new RFIDPickResponse(true);
        }
    }
}