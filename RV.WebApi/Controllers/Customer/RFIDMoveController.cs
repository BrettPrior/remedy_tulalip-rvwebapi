﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers.Customer
{
    public class RFIDMoveController : ApiController
    {
        /// <summary>
        /// Pass AssetRFIDs to move
        /// </summary>
        public class RFIDMoveRequest
        {
            public string Location { get; set; }
            public List<string> TagIDs { get; set; }
        }

        public class RFIDMoveResponse
        {
            public RFIDMoveResponse(bool success) { Success = success; }
            public RFIDMoveResponse(bool success, string errorMessage) { Success = success; ErrorMessage = errorMessage; }
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
        }

        // POST api/RFIDMove
        public RFIDMoveResponse Post([FromBody]RFIDMoveRequest request)
        {
            if (request.TagIDs == null || request.TagIDs.Count == 0)
            {
                return new RFIDMoveResponse(false, "TagIDs not passed");
            }

            var context = new RFID_RVEntities();

            Location location = null;
            if (request.Location != null && request.Location != "")
            {
                location = context.Locations.Where(l => l.Deleted == false && l.LocationName == request.Location).SingleOrDefault();
            }

            if (location == null)
            {
                // Lookup Location based on Config.DefaultLocation.Inventory
                location = context.Locations
                    .Join(context.Configs, loc => loc.LocationName, cfg => cfg.SettingValue, (loc, cfg) => new { Locations = loc, Configs = cfg })
                    .Where(l => l.Locations.Deleted == false)
                    .Where(c => c.Configs.SettingName == "DefaultLocation.Inventory")
                    .Select(l => l.Locations).SingleOrDefault();
            }

            if (location == null) { return new RFIDMoveResponse(false, $"Default Move Location not configured"); }

            foreach (string tagID in request.TagIDs)
            {
                context.spCust_Move(tagID, location.LocationID);
            }

            return new RFIDMoveResponse(true);
        }
    }
}