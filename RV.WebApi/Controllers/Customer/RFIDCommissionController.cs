﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class RFIDCommissionController : ApiController
    {
        /// <summary>
        /// Pass Barcode (Attribute1) and AssetRFIDs to commission
        /// </summary>
        public class RFIDCommissionRequest
        {
            public string Barcode { get; set; }
            public List<string> TagIDs { get; set; }
        }

        public class RFIDCommissionResponse
        {
            public RFIDCommissionResponse(bool success) { Success = success; }
            public RFIDCommissionResponse(bool success, string errorMessage) { Success = success; ErrorMessage = errorMessage; }
            public bool Success { get; set; }
            public string ErrorMessage { get; set; }
        }

        // POST api/RFIDCommission
        public RFIDCommissionResponse Post([FromBody]RFIDCommissionRequest request)
        {
            if (request.Barcode == "") { return new RFIDCommissionResponse(false, "Barcode not entered"); }

            if (request.TagIDs == null || request.TagIDs.Count == 0)
            {
                return new RFIDCommissionResponse(false, "TagIDs not passed");
            }

            var context = new RFID_RVEntities();

            // Lookup Location based on Config.DefaultLocation.Receive
            var location = context.Locations
                .Join(context.Configs, loc => loc.LocationName, cfg => cfg.SettingValue, (loc, cfg) => new { Locations = loc, Configs = cfg })
                .Where(l => l.Locations.Deleted == false)
                .Where(c => c.Configs.SettingName == "DefaultLocation.Receive")
                .Select(l => l.Locations).SingleOrDefault();

            if (location == null) { return new RFIDCommissionResponse(false, $"Default Receive Location not configured"); }

            foreach (string tagID in request.TagIDs)
            {
                context.spCust_Commission(tagID, request.Barcode, location.LocationID);
            }

            return new RFIDCommissionResponse(true);
        }
    }
}