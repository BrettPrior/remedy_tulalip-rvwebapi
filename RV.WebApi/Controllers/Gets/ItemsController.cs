﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class ItemsController : ApiController
    {
        public class GetRequest
        {
            public string ItemNumber { get; set; }
        }

        public class GetResponse
        {
            public List<Models.ItemMaster> Items { get; set; }

            public GetResponse(List<Models.ItemMaster> _items)
            { Items = _items; }
        }

        // GET api/items
        public List<Models.ItemMaster> GetByID([FromUri]long ItemID)
        {
            var context = new RFID_RVEntities();
            var rc = context.ItemMasters
                .Where(i => i.ItemID == ItemID)
                .ToList();

            return rc;
        }

        public List<Models.ItemMaster> Get([FromUri]GetRequest ItemRequest)
        {
            var context = new RFID_RVEntities();
            var rc = context.ItemMasters
                .Where(i => i.Deleted == false)
                .Where(i => ItemRequest.ItemNumber == null || i.ItemNumber == ItemRequest.ItemNumber)
                .OrderBy(i => i.ItemID)
                .ToList();

            return rc;
        }
    }
}