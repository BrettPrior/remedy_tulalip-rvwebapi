﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RV.WebApi.Helpers;
using RV.WebApi.Models;

namespace RV.WebApi.Controllers
{
    public class AssetsController : ApiController
    {
        public class GetRequest
        {
            public string AssetRFID { get; set; }
            public string Attribute1 { get; set; }
            public string Attribute2 { get; set; }
            public string Attribute3 { get; set; }
            public string Attribute4 { get; set; }
            public string Attribute5 { get; set; }
            public string Attribute6 { get; set; }
            public string Attribute7 { get; set; }
            public string Attribute8 { get; set; }
            public string Attribute9 { get; set; }
            public string Attribute10 { get; set; }
            public string Attribute11 { get; set; }
            public string Attribute12 { get; set; }
            public string Attribute13 { get; set; }
            public string Attribute14 { get; set; }
            public string Attribute15 { get; set; }
        }

        public class GetResponse
        {
            public List<Models.Asset> Assets { get; set; }

            public GetResponse(List<Models.Asset> _assets)
            { Assets = _assets; }
        }

        // GET api/assets
        public List<Models.Asset> GetByAssetID([FromUri]long AssetID)
        {
            var context = new RFID_RVEntities();
            var assets = context.Assets
                .Where(a => a.AssetID == AssetID)
                .ToList();

            return assets;
        }

        public List<Models.Asset> Get([FromUri]GetRequest AssetRequest)
        {
            var context = new RFID_RVEntities();
            var assets = context.Assets
                .Where(a => a.Deleted == false)
                .Where(a => AssetRequest.AssetRFID == null || a.AssetRFID == AssetRequest.AssetRFID)
                .Where(a => AssetRequest.Attribute1 == null || a.Attribute1 == AssetRequest.Attribute1)
                .Where(a => AssetRequest.Attribute2 == null || a.Attribute2 == AssetRequest.Attribute2)
                .Where(a => AssetRequest.Attribute3 == null || a.Attribute3 == AssetRequest.Attribute3)
                .Where(a => AssetRequest.Attribute4 == null || a.Attribute4 == AssetRequest.Attribute4)
                .Where(a => AssetRequest.Attribute5 == null || a.Attribute5 == AssetRequest.Attribute5)
                .Where(a => AssetRequest.Attribute6 == null || a.Attribute6 == AssetRequest.Attribute6)
                .Where(a => AssetRequest.Attribute7 == null || a.Attribute7 == AssetRequest.Attribute7)
                .Where(a => AssetRequest.Attribute8 == null || a.Attribute8 == AssetRequest.Attribute8)
                .Where(a => AssetRequest.Attribute9 == null || a.Attribute9 == AssetRequest.Attribute9)
                .Where(a => AssetRequest.Attribute10 == null || a.Attribute10 == AssetRequest.Attribute10)
                .OrderBy(a => a.AssetID)
                .ToList();

            return assets;
        }
    }
}