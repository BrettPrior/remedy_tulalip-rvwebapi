﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class PhysicalHeadersController : ApiController
    {
        public class GetRequest
        {
            public Nullable<long> PhysicalID { get; set; }
            public string CountReference { get; set; }
            public Nullable<DateTime> CreateDate { get; set; }
            public Nullable<bool> IsComplete { get; set; }
            public Nullable<long> CreatedBy { get; set; }
        }

        public class GetResponse
        {
            public List<PhysicalHeader> PhysicalHeader { get; set; }

            public GetResponse(List<PhysicalHeader> _physicalHeader)
            { PhysicalHeader = _physicalHeader; }
        }

        // GET api/physicalheader
        public List<PhysicalHeader> Get([FromUri]GetRequest PhysicalHeaderRequest)
        {
            var context = new RFID_RVEntities();
            var headers = context.PhysicalHeaders
                .Where(p => PhysicalHeaderRequest.PhysicalID == null || p.PhysicalID == PhysicalHeaderRequest.PhysicalID)
                .Where(p => PhysicalHeaderRequest.CountReference == null || p.CountReference == PhysicalHeaderRequest.CountReference)
                .Where(p => PhysicalHeaderRequest.CreateDate == null || p.CreateDate == PhysicalHeaderRequest.CreateDate)
                .Where(p => PhysicalHeaderRequest.IsComplete == null || p.IsComplete == PhysicalHeaderRequest.IsComplete)
                .Where(p => PhysicalHeaderRequest.CreatedBy == null || p.CreatedBy == PhysicalHeaderRequest.CreatedBy)
                .Where(p => context.PhysicalLocations.Where(l => l.PhysicalID == p.PhysicalID).Count() > 0)
                .OrderBy(p => p.PhysicalID)
                .ToList();

            return headers;
        }
    }
}