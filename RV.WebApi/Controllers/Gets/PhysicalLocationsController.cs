﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class PhysicalLocationsController : ApiController
    {
        public class GetRequest
        {
            public Nullable<long> PhysicalID { get; set; }
            public Nullable<long> PhysicalLocationID { get; set; }
            public Nullable<long> LocationID { get; set; }
            public string LocationName { get; set; }
        }

        public class GetResponse
        {
            public long PhysicalLocationID { get; set; }
            public long PhysicalID { get; set; }
            public Nullable<long> LocationID { get; set; }
            public string LocationName { get; set; }
        }

        // GET api/physicallocations
        public List<GetResponse> Get([FromUri]GetRequest PhysicalLocationRequest)
        {
            var context = new RFID_RVEntities();
            var qry = context.vwPhysicalLocations
                .Where(p => PhysicalLocationRequest.PhysicalLocationID == null || p.PhysicalLocationID == PhysicalLocationRequest.PhysicalLocationID)
                .Where(p => PhysicalLocationRequest.PhysicalID == null || p.PhysicalID == PhysicalLocationRequest.PhysicalID)
                .Where(p => PhysicalLocationRequest.LocationID == null || p.LocationID == PhysicalLocationRequest.LocationID)
                .Where(p => PhysicalLocationRequest.LocationName == null || p.LocationName == PhysicalLocationRequest.LocationName)
                .OrderBy(p => p.PhysicalID)
                .ToList();
                //.FirstOrDefault();

            List<GetResponse> rc = new List<GetResponse>(); 

            foreach(Models.vwPhysicalLocation rec in qry)
            {
                rc.Add(new GetResponse() { LocationName = rec.LocationName, LocationID = rec.LocationID, PhysicalID = rec.PhysicalID, PhysicalLocationID = rec.PhysicalLocationID });
            }

            return rc;
        }
    }
}