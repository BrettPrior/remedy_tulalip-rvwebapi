﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{ 
    public class PhysicalCollectionController : ApiController
    {
        public class GetRequest
        {
            public Nullable<long> PhysicalID { get; set; }
        }

        public class GetResponse
        {
            public List<CountAsset> CountAssets;
            public List<CountHH> CountHH;
            public List<CountLocation> CountLocations;
            public List<CountPC> CountPC;
        }

        public class CountLocation
        {
            public long PhysicalLocationID { get; set; }
            public long PhysicalID { get; set; }
            public Nullable<long> LocationID { get; set; }
            public string LocationName { get; set; }
        }

        public class CountPC
        {
            public long PhysicalPCID { get; set; }
            public long PhysicalLocationID { get; set; }
            public long PhysicalID { get; set; }
            public Nullable<long> AssetID { get; set; }
        }

        public class CountHH
        {
            public long PhysicalHHID { get; set; }
            public long PhysicalLocationID { get; set; }
            public long PhysicalID { get; set; }
            public Nullable<long> AssetID { get; set; }
        }

        public class CountAsset
        {
            public long AssetID { get; set; }
            public string AssetRFID { get; set; }
            public Nullable<System.DateTime> CreationDate { get; set; }
            public string AssetType { get; set; }
            public string Attribute1 { get; set; }
            public string Attribute2 { get; set; }
            public string Attribute3 { get; set; }
            public string Attribute4 { get; set; }
            public string Attribute5 { get; set; }
            public string Attribute6 { get; set; }
            public string Attribute7 { get; set; }
            public string Attribute8 { get; set; }
            public string Attribute9 { get; set; }
            public string Attribute10 { get; set; }
            public string Location { get; set; }
            public string ItemNumber { get; set; }
            public string ItemDescription { get; set; }
            public string ItemCFld1 { get; set; }
            public string ItemCFld2 { get; set; }
            public string ItemCFld3 { get; set; }
            public string ItemCFld4 { get; set; }
            public string ItemCFld5 { get; set; }
            public string ItemCFld6 { get; set; }
            public string ItemCFld7 { get; set; }
            public string ItemCFld8 { get; set; }
        }

        // GET api/physicalheader
        public GetResponse Get([FromUri]GetRequest apiRequest)
        {
            GetResponse rc = new GetResponse();

            var context = new RFID_RVEntities();

            rc.CountHH = new List<CountHH>();
            rc.CountLocations = new List<CountLocation>();
            rc.CountPC = new List<CountPC>();

            var locations = context.vwPhysicalLocations
                .Where(p => apiRequest.PhysicalID == null || p.PhysicalID == apiRequest.PhysicalID)
                .OrderBy(p => p.LocationName)
                .ToList();
            foreach (Models.vwPhysicalLocation rec in locations)
            {
                rc.CountLocations.Add(new CountLocation() { LocationName = rec.LocationName, LocationID = rec.LocationID, PhysicalID = rec.PhysicalID, PhysicalLocationID = rec.PhysicalLocationID });
            }

            var pcs = context.PhysicalPCs
                .Where(p => p.PhysicalID == apiRequest.PhysicalID)
                .ToList();
            foreach (Models.PhysicalPC rec in pcs)
            {
                rc.CountPC.Add(new CountPC() { AssetID = rec.AssetID, PhysicalID = rec.PhysicalID, PhysicalLocationID = (rec.PhysicalLocationID == null) ? 0 : rec.PhysicalLocationID.Value, PhysicalPCID = rec.PhysicalPCID });
            }

            var hhs = context.PhysicalHHs
                .Where(p => p.PhysicalID == apiRequest.PhysicalID)
                .ToList();
            foreach (Models.PhysicalHH rec in hhs)
            {
                rc.CountHH.Add(new CountHH() { AssetID = rec.AssetID, PhysicalID = rec.PhysicalID, PhysicalLocationID = (rec.PhysicalLocationID == null) ? 0 : rec.PhysicalLocationID.Value, PhysicalHHID = rec.PhysicalHHID });
            }

            var assets = (from vwpcs in context.vwPhysicalPCs
                          where vwpcs.PhysicalID == apiRequest.PhysicalID
                          select new CountAsset()
                          {
                              AssetID = vwpcs.AssetID.Value,
                              AssetRFID = vwpcs.AssetRFID,
                              AssetType = vwpcs.AssetType,
                              Attribute1 = vwpcs.Attribute1,
                              Attribute2 = vwpcs.Attribute2,
                              Attribute3 = vwpcs.Attribute3,
                              Attribute4 = vwpcs.Attribute4,
                              Attribute5 = vwpcs.Attribute5,
                              Attribute6 = vwpcs.Attribute6,
                              Attribute7 = vwpcs.Attribute7,
                              Attribute8 = vwpcs.Attribute8,
                              Attribute9 = vwpcs.Attribute9,
                              Attribute10 = vwpcs.Attribute10,
                              CreationDate = vwpcs.CreateDate,
                              Location = vwpcs.Location,
                              ItemCFld1 = vwpcs.ItemCFld1,
                              ItemCFld2 = vwpcs.ItemCFld2,
                              ItemCFld3 = vwpcs.ItemCFld3,
                              ItemCFld4 = vwpcs.ItemCFld4,
                              ItemCFld5 = vwpcs.ItemCFld5,
                              ItemCFld6 = vwpcs.ItemCFld6,
                              ItemCFld7 = vwpcs.ItemCFld7,
                              ItemCFld8 = vwpcs.ItemCFld8,
                              ItemDescription = vwpcs.ItemDescription,
                              ItemNumber = vwpcs.ItemNumber
                          })
                           .Union
                           (from vwhhs in context.vwPhysicalHHs
                            where vwhhs.PhysicalID == apiRequest.PhysicalID
                            select new CountAsset()
                            {
                                AssetID = vwhhs.AssetID.Value,
                                AssetRFID = vwhhs.AssetRFID,
                                AssetType = vwhhs.AssetType,
                                Attribute1 = vwhhs.Attribute1,
                                Attribute2 = vwhhs.Attribute2,
                                Attribute3 = vwhhs.Attribute3,
                                Attribute4 = vwhhs.Attribute4,
                                Attribute5 = vwhhs.Attribute5,
                                Attribute6 = vwhhs.Attribute6,
                                Attribute7 = vwhhs.Attribute7,
                                Attribute8 = vwhhs.Attribute8,
                                Attribute9 = vwhhs.Attribute9,
                                Attribute10 = vwhhs.Attribute10,
                                CreationDate = vwhhs.CreateDate,
                                Location = vwhhs.Location,
                                ItemCFld1 = vwhhs.ItemCFld1,
                                ItemCFld2 = vwhhs.ItemCFld2,
                                ItemCFld3 = vwhhs.ItemCFld3,
                                ItemCFld4 = vwhhs.ItemCFld4,
                                ItemCFld5 = vwhhs.ItemCFld5,
                                ItemCFld6 = vwhhs.ItemCFld6,
                                ItemCFld7 = vwhhs.ItemCFld7,
                                ItemCFld8 = vwhhs.ItemCFld8,
                                ItemDescription = vwhhs.ItemDescription,
                                ItemNumber = vwhhs.ItemNumber
                            }).ToList();
            rc.CountAssets = (List<CountAsset>)assets;
            return rc;
        }
    }
}