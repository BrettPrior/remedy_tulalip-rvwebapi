﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class PhysicalHHController : ApiController
    {
        public class GetRequest
        {
            public Nullable<long> PhysicalID { get; set; }
            public Nullable<long> PhysicalLocationID { get; set; }
            public Nullable<long> AssetID { get; set; }
        }

        public class GetResponse
        {
            public long PhysicalHHID { get; set; }
            public long PhysicalLocationID { get; set; }
            public long PhysicalID { get; set; }
            public Nullable<long> AssetID { get; set; }
            public string AssetRFID { get; set; }
        }

        // GET api/physicalhh
        public List<GetResponse> Get([FromUri]GetRequest PhysicalHHRequest)
        {
            var context = new RFID_RVEntities();
            var qry = context.vwPhysicalHHs
                .Where(p => PhysicalHHRequest.PhysicalLocationID == null || p.PhysicalLocationID == PhysicalHHRequest.PhysicalLocationID)
                .Where(p => PhysicalHHRequest.PhysicalID == null || p.PhysicalID == PhysicalHHRequest.PhysicalID)
                .Where(p => PhysicalHHRequest.AssetID == null || p.AssetID == PhysicalHHRequest.AssetID)
                .OrderBy(p => p.PhysicalHHID)
                .ToList();

            List<GetResponse> rc = new List<GetResponse>();

            foreach (Models.vwPhysicalHH rec in qry)
            {
                rc.Add(new GetResponse() { AssetID = rec.AssetID, AssetRFID = rec.AssetRFID, PhysicalID = rec.PhysicalID, PhysicalHHID = rec.PhysicalHHID, PhysicalLocationID = rec.PhysicalID });
            }

            return rc;
        }
    }
}