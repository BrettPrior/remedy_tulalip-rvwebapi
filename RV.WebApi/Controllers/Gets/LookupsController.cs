﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class LookupsController : ApiController
    {
        public class GetResponse
        {
            public List<Models.Lookup> Lookup { get; set; }

            public GetResponse(List<Models.Lookup> _lookup)
            { Lookup = _lookup; }
        }

        // GET api/lookups
        public List<Models.Lookup> Get()
        {
            var context = new RFID_RVEntities();
            var rc = context.Lookups
                .ToList();

            return rc;
        }

        public List<Models.Lookup> GetByType([FromUri]string LookupType)
        {
            var context = new RFID_RVEntities();
            var rc = context.Lookups
                .Where(a => a.LookupType == LookupType)
                .ToList();

            return rc;
        }
    }
}