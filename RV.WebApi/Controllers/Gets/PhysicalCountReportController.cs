﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class PhysicalCountReportController : ApiController
    {
        public class GetRequest
        {
            public Nullable<long> PhysicalID { get; set; }
            public string CountReference { get; set; }
            public Nullable<DateTime> CreateDate { get; set; }
            public Nullable<bool> IsComplete { get; set; }
            public Nullable<long> CreatedBy { get; set; }
            public string ReportType { get; set; }
        }

        public class GetDetailResponse
        {
            public string CountStatus { get; set; }
            public string CountReference { get; set; }
            public DateTime? DateCreated { get; set; }
            public DateTime? DateCounted { get; set; }
            public string AssetRFID { get; set; }
            public string AssetType { get; set; }
            public string ExpectedLocation { get; set; }
            public string CountLocation { get; set; }
            public string Attribute1 { get; set; }
            public string Attribute2 { get; set; }
            public string Attribute3 { get; set; }
            public string Attribute4 { get; set; }
            public string Attribute5 { get; set; }
            public string Attribute6 { get; set; }
            public string Attribute7 { get; set; }
            public string Attribute8 { get; set; }
            public string Attribute9 { get; set; }
            public string Attribute10 { get; set; }
            public string Attribute11 { get; set; }
            public string Attribute12 { get; set; }
            public string Attribute13 { get; set; }
            public string Attribute14 { get; set; }
            public string Attribute15 { get; set; }
            public string ItemCFld1 { get; set; }
            public string ItemCFld2 { get; set; }
            public string ItemCFld3 { get; set; }
            public string ItemCFld4 { get; set; }
            public string ItemCFld5 { get; set; }
            public string ItemCFld6 { get; set; }
            public string ItemCFld7 { get; set; }
            public string ItemCFld8 { get; set; }
            public string CountedBy { get; set; }
        }

        public class GetSummaryResponse
        {
            public string CountReference { get; set; }
            public string ItemNumber { get; set; }
            public string ItemDescription { get; set; }
            public string Location { get; set; }
            public string QtyFound { get; set; }
            public string QtyMissing { get; set; }
            public string QtyUnexpected { get; set; }
        }

        public class GetSummaryByCountReferenceResponse
        {
            public string CountReference { get; set; }
            public string QtyFound { get; set; }
            public string QtyMissing { get; set; }
            public string QtyUnexpected { get; set; }
        }

        // GET api/physicalCountReport

        public object Get([FromUri]GetRequest request)
        {
            object rc = null;

            var context = new RFID_RVEntities();

            if (request.ReportType?.ToLower() == "detail")
            {
                List<GetDetailResponse> detailResponses = new List<GetDetailResponse>();

                if (request.IsComplete == null) { request.IsComplete = false; }

                var openCounts = context.PhysicalHeaders
                    .Where(p => request.PhysicalID == null || p.PhysicalID == request.PhysicalID)
                    .Where(p => request.CountReference == null || p.CountReference == request.CountReference)
                    .Where(p => request.CreateDate == null || p.CreateDate == request.CreateDate)
                    .Where(p => request.IsComplete == null || p.IsComplete == request.IsComplete)
                    .Where(p => request.CreatedBy == null || p.CreatedBy == request.CreatedBy)
                    .OrderBy(p => p.CountReference)
                    .Select(p => p.PhysicalID);

                foreach (long physicalID in openCounts)
                {
                    var counts = context.fnPhysicalInventoryVariance(physicalID).ToList();
                    if (counts != null)
                    {
                        foreach (fnPhysicalInventoryVariance_Result count in counts)
                        {
                            detailResponses.Add(new GetDetailResponse()
                            {
                                CountReference = count.Count_Reference,
                                AssetRFID = count.C_Asset__RFID,
                                AssetType = count.C_Asset__Type,
                                Attribute1 = count.C_Attribute1_,
                                Attribute2 = count.C_Attribute2_,
                                Attribute3 = count.C_Attribute3_,
                                Attribute4 = count.C_Attribute4_,
                                Attribute5 = count.C_Attribute5_,
                                Attribute6 = count.C_Attribute6_,
                                Attribute7 = count.C_Attribute7_,
                                Attribute8 = count.C_Attribute8_,
                                Attribute9 = count.C_Attribute9_,
                                Attribute10 = count.C_Attribute10_,
                                CountedBy = count.Counted_By,
                                CountLocation = count.Count_Location,
                                CountStatus = count.Count_Status,
                                DateCounted = count.Date_Counted,
                                DateCreated = count.Date_Created,
                                ExpectedLocation = count.Location,
                                ItemCFld1 = count.C_ItemCFld1_,
                                ItemCFld2 = count.C_ItemCFld2_,
                                ItemCFld3 = count.C_ItemCFld3_,
                                ItemCFld4 = count.C_ItemCFld4_,
                                ItemCFld5 = count.C_ItemCFld5_,
                                ItemCFld6 = count.C_ItemCFld6_,
                                ItemCFld7 = count.C_ItemCFld7_,
                                ItemCFld8 = count.C_ItemCFld8_
                            });

                        }
                    }
                }
                rc = detailResponses;

            }
            else if (request.ReportType?.ToLower() == "summary")
            {
                List<GetSummaryResponse> summaryResponses = new List<GetSummaryResponse>();

                if (request.IsComplete == null) { request.IsComplete = false; }

                var openCounts = context.PhysicalHeaders
                    .Where(p => request.PhysicalID == null || p.PhysicalID == request.PhysicalID)
                    .Where(p => request.CountReference == null || p.CountReference == request.CountReference)
                    .Where(p => request.CreateDate == null || p.CreateDate == request.CreateDate)
                    .Where(p => request.IsComplete == null || p.IsComplete == request.IsComplete)
                    .Where(p => request.CreatedBy == null || p.CreatedBy == request.CreatedBy)
                    .OrderBy(p => p.CountReference)
                    .Select(p => p.PhysicalID);

                foreach (long physicalID in openCounts)
                {
                    var sums = context.fnPhysicalInventoryVariance_Summary(physicalID).ToList();
                    if (sums != null)
                    {
                        foreach (fnPhysicalInventoryVariance_Summary_Result sum in sums)
                        {
                            summaryResponses.Add(new GetSummaryResponse()
                            {
                                CountReference = sum.Count_Reference,
                                ItemDescription = sum.C_Item__Desc,
                                ItemNumber = sum.C_ItemNumber_,
                                Location = sum.Location,
                                QtyFound = sum.Qty_Found.ToString(),
                                QtyMissing = sum.Qty_Missing.ToString(),
                                QtyUnexpected = sum.Qty_Unexpected.ToString()
                            });

                        }
                    }
                }
                rc = summaryResponses;
            }
            else if (request.ReportType?.ToLower() == "summarybycountreference")
            {
                List<GetSummaryByCountReferenceResponse> summaryResponses = new List<GetSummaryByCountReferenceResponse>();

                if (request.IsComplete == null) { request.IsComplete = false; }

                var openCounts = context.PhysicalHeaders
                    .Where(p => request.PhysicalID == null || p.PhysicalID == request.PhysicalID)
                    .Where(p => request.CountReference == null || p.CountReference == request.CountReference)
                    .Where(p => request.CreateDate == null || p.CreateDate == request.CreateDate)
                    .Where(p => request.IsComplete == null || p.IsComplete == request.IsComplete)
                    .Where(p => request.CreatedBy == null || p.CreatedBy == request.CreatedBy)
                    .OrderBy(p => p.CountReference)
                    .Select(p => p.PhysicalID);

                foreach (long physicalID in openCounts)
                {
                    var sums = context.fnPhysicalInventoryVariance_Summary(physicalID);

                    var sums2 = sums.GroupBy(p => p.Count_Reference)
                        .Select(s => new {
                            Count_Reference = s.Key,
                            Qty_Found = s.Sum(s2 => s2.Qty_Found),
                            Qty_Missing = s.Sum(s2 => s2.Qty_Missing),
                            Qty_Unexpected = s.Sum(s2 => s2.Qty_Unexpected)
                        }).FirstOrDefault();

                    if (sums2 != null)
                    {
                        summaryResponses.Add(new GetSummaryByCountReferenceResponse()
                        {
                            CountReference = sums2.Count_Reference,
                            QtyFound = sums2.Qty_Found.ToString(),
                            QtyMissing = sums2.Qty_Missing.ToString(),
                            QtyUnexpected = sums2.Qty_Unexpected.ToString()
                        });
                    }
                }
                rc = summaryResponses;
            }
            return rc;
        }
    }
}