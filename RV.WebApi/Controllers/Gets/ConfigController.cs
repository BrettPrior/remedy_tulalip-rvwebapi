﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class ConfigController : ApiController
    {
        public class GetResponse
        {
            public List<Models.Config> Config { get; set; }

            public GetResponse(List<Models.Config> _config)
            { Config = _config; }
        }

        // GET api/config
        public List<Models.Config> Get()
        {
            var context = new RFID_RVEntities();
            var rc = context.Configs
                .ToList();

            return rc;
        }

        public List<Models.Config> GetByName([FromUri]string SettingName)
        {
            var context = new RFID_RVEntities();
            var rc = context.Configs
                .Where(a => a.SettingName == SettingName)
                .ToList();

            return rc;
        }
    }
}