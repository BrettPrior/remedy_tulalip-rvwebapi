﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class PhysicalPCController : ApiController
    {
        public class GetRequest
        {
            public Nullable<long> PhysicalID { get; set; }
            public Nullable<long> PhysicalLocationID { get; set; }
            public Nullable<long> AssetID { get; set; }
        }

        public class GetResponse
        {
            public long PhysicalPCID { get; set; }
            public long PhysicalLocationID { get; set; }
            public long PhysicalID { get; set; }
            public Nullable<long> AssetID { get; set; }
            public string AssetRFID { get; set; }
        }

        // GET api/physicalpc
        public List<GetResponse> Get([FromUri]GetRequest PhysicalPCRequest)
        {
            var context = new RFID_RVEntities();
            var qry = context.vwPhysicalPCs
                .Where(p => PhysicalPCRequest.PhysicalLocationID == null || p.PhysicalLocationID == PhysicalPCRequest.PhysicalLocationID)
                .Where(p => PhysicalPCRequest.PhysicalID == null || p.PhysicalID == PhysicalPCRequest.PhysicalID)
                .Where(p => PhysicalPCRequest.AssetID == null || p.AssetID == PhysicalPCRequest.AssetID)
                .OrderBy(p => p.PhysicalPCID)
                .ToList();

            List<GetResponse> rc = new List<GetResponse>();

            foreach (Models.vwPhysicalPC rec in qry)
            {
                rc.Add(new GetResponse() { AssetID = rec.AssetID, AssetRFID = rec.AssetRFID, PhysicalID = rec.PhysicalID, PhysicalPCID = rec.PhysicalPCID, PhysicalLocationID = rec.PhysicalID });
            }

            return rc;
        }
    }
}