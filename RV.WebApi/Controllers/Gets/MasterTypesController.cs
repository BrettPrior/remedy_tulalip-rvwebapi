using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class MasterTypesController : ApiController
    {
        public class GetRequest
        {
            public Nullable<long> MasterTypeID { get; set; }
            public string MasterType { get; set; }
        }

        public class GetResponse
        {
            public List<Models.MasterType> MasterTypes { get; set; }

            public GetResponse(List<Models.MasterType> _masterType)
            { MasterTypes = _masterType; }
        }

        // GET api/config
        public List<Models.MasterType> Get([FromUri]GetRequest MasterTypeRequest)
        {
            var context = new RFID_RVEntities();
            var rc = context.MasterTypes
                .Where(a => MasterTypeRequest.MasterTypeID == null || a.MasterTypeID == MasterTypeRequest.MasterTypeID)
                .Where(a => MasterTypeRequest.MasterType == null || a.MasterType1 == MasterTypeRequest.MasterType)
                .ToList();

            return rc;
        }
    }
}