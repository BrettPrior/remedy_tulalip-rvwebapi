using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class AssetTypesController : ApiController
    {
        public class GetRequest
        {
            public Nullable<long> AssetTypeID { get; set; }
            public string AssetType { get; set; }
        }

        public class GetResponse
        {
            public List<Models.AssetType> AssetTypes { get; set; }

            public GetResponse(List<Models.AssetType> _assetTypes)
            { AssetTypes = _assetTypes; }
        }

        // GET api/AssetTypes
        public List<Models.AssetType> Get([FromUri]GetRequest AssetTypeRequest)
        {
            var context = new RFID_RVEntities();
            var rc = context.AssetTypes
                .Where(a => a.Deleted == false)
                .Where(a => AssetTypeRequest.AssetTypeID == null || a.AssetTypeID == AssetTypeRequest.AssetTypeID)
                .Where(a => AssetTypeRequest.AssetType == null || a.AssetType1 == AssetTypeRequest.AssetType)
                .ToList();

            return rc;
        }
    }
}