﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class LocationsController : ApiController
    {
        public class GetResponse
        {
            public List<Models.Location> Locations { get; set; }

            public GetResponse(List<Models.Location> _locations)
            { Locations = _locations; }
        }

        public class GetRequest
        {
            public string Location { get; set; }
            public bool? IsMove { get; set; }
            public bool? IsReceive { get; set; }
            public bool? IsShip { get; set; }
            public bool? IsPick { get; set; }
            public bool? IsKill { get; set; }
            public bool? IsValidation { get; set; }
            public bool? IsDestination { get; set; }
            public bool? IsCustom1 { get; set; }
            public bool? IsCustom2 { get; set; }
            public bool? IsCustom3 { get; set; }
            public bool? IsCustom4 { get; set; }
            public bool? IsCustom5 { get; set; }
            public int? MaxRecs { get; set; }
            public int? SkipRecs { get; set; }
        }

        // GET api/locations
        public List<Models.Location> GetByID([FromUri]long LocationID)
        {
            var context = new RFID_RVEntities();
            var rc = context.Locations
                .Where(l => l.LocationID == LocationID)
                .ToList();

            return rc;
        }

        public List<Models.Location> Get([FromUri]GetRequest LocationRequest)
        {
            List<Location> rc;

            var context = new RFID_RVEntities();
            if (LocationRequest.MaxRecs != null)
            {
                rc = context.Locations
                    .Where(l => l.Deleted == false)
                    .Where(l => LocationRequest.Location == null || l.LocationName == LocationRequest.Location)
                    .Where(l => LocationRequest.IsCustom1 == null || l.IsCustom1 == LocationRequest.IsCustom1)
                    .Where(l => LocationRequest.IsCustom2 == null || l.IsCustom2 == LocationRequest.IsCustom2)
                    .Where(l => LocationRequest.IsCustom3 == null || l.IsCustom3 == LocationRequest.IsCustom3)
                    .Where(l => LocationRequest.IsCustom4 == null || l.IsCustom4 == LocationRequest.IsCustom4)
                    .Where(l => LocationRequest.IsCustom5 == null || l.IsCustom5 == LocationRequest.IsCustom5)
                    .Where(l => LocationRequest.IsDestination == null || l.IsDestination == LocationRequest.IsDestination)
                    .Where(l => LocationRequest.IsKill == null || l.IsKill == LocationRequest.IsKill)
                    .Where(l => LocationRequest.IsMove == null || l.IsMove == LocationRequest.IsMove)
                    .Where(l => LocationRequest.IsPick == null || l.IsPick == LocationRequest.IsPick)
                    .Where(l => LocationRequest.IsReceive == null || l.IsReceive == LocationRequest.IsReceive)
                    .Where(l => LocationRequest.IsShip == null || l.IsShip == LocationRequest.IsShip)
                    .Where(l => LocationRequest.IsValidation == null || l.IsValidation == LocationRequest.IsValidation)
                    .OrderBy(l => l.LocationName)
                    .Skip((LocationRequest.SkipRecs == null) ? 0 : (int)LocationRequest.SkipRecs)
                    .Take((int)LocationRequest.MaxRecs)
                    .ToList();
            }
            else
            {
                rc = context.Locations
                    .Where(l => l.Deleted == false)
                    .Where(l => LocationRequest.Location == null || (!LocationRequest.Location.Contains("*")  && l.LocationName == LocationRequest.Location) || (LocationRequest.Location.Contains("*") && l.LocationName.Contains(LocationRequest.Location.Replace("*",""))))
                    .Where(l => LocationRequest.IsCustom1 == null || l.IsCustom1 == LocationRequest.IsCustom1)
                    .Where(l => LocationRequest.IsCustom2 == null || l.IsCustom2 == LocationRequest.IsCustom2)
                    .Where(l => LocationRequest.IsCustom3 == null || l.IsCustom3 == LocationRequest.IsCustom3)
                    .Where(l => LocationRequest.IsCustom4 == null || l.IsCustom4 == LocationRequest.IsCustom4)
                    .Where(l => LocationRequest.IsCustom5 == null || l.IsCustom5 == LocationRequest.IsCustom5)
                    .Where(l => LocationRequest.IsDestination == null || l.IsDestination == LocationRequest.IsDestination)
                    .Where(l => LocationRequest.IsKill == null || l.IsKill == LocationRequest.IsKill)
                    .Where(l => LocationRequest.IsMove == null || l.IsMove == LocationRequest.IsMove)
                    .Where(l => LocationRequest.IsPick == null || l.IsPick == LocationRequest.IsPick)
                    .Where(l => LocationRequest.IsReceive == null || l.IsReceive == LocationRequest.IsReceive)
                    .Where(l => LocationRequest.IsShip == null || l.IsShip == LocationRequest.IsShip)
                    .Where(l => LocationRequest.IsValidation == null || l.IsValidation == LocationRequest.IsValidation)
                    .OrderBy(l => l.LocationName)
                    .Skip((LocationRequest.SkipRecs == null) ? 0 : (int)LocationRequest.SkipRecs)
                    .ToList();
            }
            return rc;
        }
    }
}