﻿using RV.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace RV.WebApi.Controllers
{
    public class PromptsController : ApiController
    {
        public class GetResponse
        {
            public List<Models.Prompt> Prompt { get; set; }

            public GetResponse(List<Models.Prompt> _prompt)
            { Prompt = _prompt; }
        }

        // GET api/prompt
        public List<Models.Prompt> Get()
        {
            var context = new RFID_RVEntities();
            var rc = context.Prompts
                .ToList();

            return rc;
        }

        public List<Models.Prompt> GetByLabel([FromUri]string PromptLabel)
        {
            var context = new RFID_RVEntities();
            var rc = context.Prompts
                .Where(a => a.PromptLabel == PromptLabel)
                .ToList();

            return rc;
        }
    }
}