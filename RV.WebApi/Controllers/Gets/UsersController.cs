﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RV.WebApi.Helpers;
using RV.WebApi.Models;


namespace RV.WebApi.Controllers
{
    public class UsersController : ApiController
    {
        public class GetRequest
        {
            public string UserLogin { get; set; }
            public string UserName { get; set; }
            public long? GroupID { get; set; }
            public string GroupName { get; set; }
        }

        public class GetResponse
        {
            public List<Models.vwUser> Users { get; set; }

            public GetResponse(List<Models.vwUser> _users)
            { Users = _users; }
        }

        // GET api/users
        public List<Models.vwUser> GetByID([FromUri]long UserID)
        {
            var context = new RFID_RVEntities();
            var rc = context.vwUsers
                .Where(l => l.UserID == UserID)
                .ToList();

            return rc;
        }

        public List<Models.vwUser> Get([FromUri]GetRequest UsersRequest)
        {
            List<vwUser> rc;

            var context = new RFID_RVEntities();
                rc = context.vwUsers
                    .Where(u => UsersRequest.UserLogin == null || (!UsersRequest.UserLogin.Contains("*") && u.UserLogin == UsersRequest.UserLogin) || (UsersRequest.UserLogin.Contains("*") && u.UserLogin.Contains(UsersRequest.UserLogin.Replace("*", ""))))
                    .Where(u => UsersRequest.UserName == null || (!UsersRequest.UserName.Contains("*") && u.UserName == UsersRequest.UserName) || (UsersRequest.UserName.Contains("*") && u.UserName.Contains(UsersRequest.UserName.Replace("*", ""))))
                    .Where(u => UsersRequest.GroupID == null || u.GroupID == UsersRequest.GroupID)
                    .Where(u => UsersRequest.GroupName == null || u.GroupName == UsersRequest.GroupName)
                    .OrderBy(u => u.UserLogin)
                    .ToList();
            return rc;
        }
    }
}