﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using RV.WebApi.Helpers;
using RV.WebApi.Models;

namespace RV.WebApi.Controllers
{
    public class vwAssetsController : ApiController
    {
        public class GetRequest
        {
            public string AssetType { get; set; }
            public string AssetRFID { get; set; }
            public string Attribute1 { get; set; }
            public string Attribute2 { get; set; }
            public string Attribute3 { get; set; }
            public string Attribute4 { get; set; }
            public string Attribute5 { get; set; }
            public string Attribute6 { get; set; }
            public string Attribute7 { get; set; }
            public string Attribute8 { get; set; }
            public string Attribute9 { get; set; }
            public string Attribute10 { get; set; }
            public string Attribute11 { get; set; }
            public string Attribute12 { get; set; }
            public string Attribute13 { get; set; }
            public string Attribute14 { get; set; }
            public string Attribute15 { get; set; }
            public string Location { get; set; }
            public string ItemNumber { get; set; }
            public string ItemCFld1 { get; set; }
            public string ItemCFld2 { get; set; }
            public string ItemCFld3 { get; set; }
            public string ItemCFld4 { get; set; }
            public string ItemCFld5 { get; set; }
            public string ItemCFld6 { get; set; }
            public string ItemCFld7 { get; set; }
            public string ItemCFld8 { get; set; }
        }

        public class GetResponse
        {
            public List<Models.vwAsset> Assets { get; set; }

            public GetResponse(List<Models.vwAsset> _assets)
            { Assets = _assets; }
        }

        // GET api/vwassets
        public List<Models.vwAsset> GetByAssetID([FromUri]long AssetID)
        {
            var context = new RFID_RVEntities();
            var rc = context.vwAssets
                .Where(a => a.AssetID == AssetID)
                .ToList();

            return rc;
        }

        public List<Models.vwAsset> GetBySearchField([FromUri]string SearchField)
        {
            var context = new RFID_RVEntities();

            var lookupFields = context.Configs.Where(c => c.SettingName == "Asset.LookupFields").SingleOrDefault().SettingValue.Split('~');

            var rc = context.vwAssets
                .Where(a => a.AssetRFID.Contains(SearchField))
                .ToList();

            if (rc.Count == 0)
                foreach (string lookupField in lookupFields)
                {
                    if (lookupField.ToLower() == "attribute1")
                        rc = context.vwAssets.Where(a => a.Attribute1.Equals(SearchField)).ToList();
                    else if (lookupField.ToLower() == "attribute2")
                        rc = context.vwAssets.Where(a => a.Attribute2.Equals(SearchField)).ToList();
                    else if (lookupField.ToLower() == "attribute3")
                        rc = context.vwAssets.Where(a => a.Attribute3.Equals(SearchField)).ToList();
                    else if (lookupField.ToLower() == "attribute4")
                        rc = context.vwAssets.Where(a => a.Attribute4.Equals(SearchField)).ToList();
                    else if (lookupField.ToLower() == "attribute5")
                        rc = context.vwAssets.Where(a => a.Attribute5.Equals(SearchField)).ToList();
                    else if (lookupField.ToLower() == "attribute6")
                        rc = context.vwAssets.Where(a => a.Attribute6.Equals(SearchField)).ToList();
                    else if (lookupField.ToLower() == "attribute7")
                        rc = context.vwAssets.Where(a => a.Attribute7.Equals(SearchField)).ToList();
                    else if (lookupField.ToLower() == "attribute8")
                        rc = context.vwAssets.Where(a => a.Attribute8.Equals(SearchField)).ToList();
                    else if (lookupField.ToLower() == "attribute9")
                        rc = context.vwAssets.Where(a => a.Attribute9.Equals(SearchField)).ToList();
                    else if (lookupField.ToLower() == "attribute10")
                        rc = context.vwAssets.Where(a => a.Attribute10.Equals(SearchField)).ToList();

                    if (rc.Count > 0) { break; }
                }

            return rc;
        }

        public List<Models.vwAsset> GetByAnything([FromUri]GetRequest vwAssetRequest)
        {
            var context = new RFID_RVEntities();
            var assets = context.vwAssets
                .Where(a => vwAssetRequest.AssetRFID == null || a.AssetRFID.Contains(vwAssetRequest.AssetRFID))
                .Where(a => vwAssetRequest.AssetType == null || a.AssetType == vwAssetRequest.AssetType)
                .Where(a => vwAssetRequest.Attribute1 == null || a.Attribute1 == vwAssetRequest.Attribute1)
                .Where(a => vwAssetRequest.Attribute2 == null || a.Attribute2 == vwAssetRequest.Attribute2)
                .Where(a => vwAssetRequest.Attribute3 == null || a.Attribute3 == vwAssetRequest.Attribute3)
                .Where(a => vwAssetRequest.Attribute4 == null || a.Attribute4 == vwAssetRequest.Attribute4)
                .Where(a => vwAssetRequest.Attribute5 == null || a.Attribute5 == vwAssetRequest.Attribute5)
                .Where(a => vwAssetRequest.Attribute6 == null || a.Attribute6 == vwAssetRequest.Attribute6)
                .Where(a => vwAssetRequest.Attribute7 == null || a.Attribute7 == vwAssetRequest.Attribute7)
                .Where(a => vwAssetRequest.Attribute8 == null || a.Attribute8 == vwAssetRequest.Attribute8)
                .Where(a => vwAssetRequest.Attribute9 == null || a.Attribute9 == vwAssetRequest.Attribute9)
                .Where(a => vwAssetRequest.Attribute10 == null || a.Attribute10 == vwAssetRequest.Attribute10)
                .Where(a => vwAssetRequest.ItemNumber == null || a.ItemNumber == vwAssetRequest.ItemNumber)
                .Where(a => vwAssetRequest.ItemCFld1 == null || a.ItemCFld1 == vwAssetRequest.ItemCFld1)
                .Where(a => vwAssetRequest.ItemCFld2 == null || a.ItemCFld2 == vwAssetRequest.ItemCFld2)
                .Where(a => vwAssetRequest.ItemCFld3 == null || a.ItemCFld3 == vwAssetRequest.ItemCFld3)
                .Where(a => vwAssetRequest.ItemCFld4 == null || a.ItemCFld4 == vwAssetRequest.ItemCFld4)
                .Where(a => vwAssetRequest.ItemCFld5 == null || a.ItemCFld5 == vwAssetRequest.ItemCFld5)
                .Where(a => vwAssetRequest.ItemCFld6 == null || a.ItemCFld6 == vwAssetRequest.ItemCFld6)
                .Where(a => vwAssetRequest.ItemCFld7 == null || a.ItemCFld7 == vwAssetRequest.ItemCFld7)
                .Where(a => vwAssetRequest.ItemCFld8 == null || a.ItemCFld8 == vwAssetRequest.ItemCFld8)
                .Where(a => vwAssetRequest.Location == null || a.Location == vwAssetRequest.Location)
                .OrderBy(a => a.AssetID)
                .ToList();

            return assets;
        }
    }
}