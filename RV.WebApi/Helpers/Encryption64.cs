﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace RV.WebApi.Helpers
{
    public class Encryption64
    {
        public string EncryptionKey { get; private set; }

        public Encryption64()
        {
            EncryptionKey = "Rf!D RUL35";
        }

        // Use DES CryptoService with Private key pair
    //Private key() As Byte = { } ' we are going to pass in the key portion in our method calls
    //Private IV() As Byte = { &H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF }

        public string Decrypt64(string stringToDecrypt )
        {
            // Note: The DES CryptoService only accepts certain key byte lengths
            // We are going to make things easy by insisting on an 8 byte legal key length

            byte[] key = { }; // we are going to pass in the key portion in our method calls
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };

            try
            {
                if (stringToDecrypt == "") { return ""; }
                key = System.Text.Encoding.UTF8.GetBytes(EncryptionKey.Substring(0, 8));
                byte[] inputByteArray = new byte[stringToDecrypt.Length];

                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                // we have a base 64 encoded string so first must decode to regular unencoded (encrypted) string
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                // now decrypt the regular string
                MemoryStream ms = new MemoryStream();

                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e) { return e.Message; }
        }
        public string Encrypt64(string stringToEncrypt)
        {
            try
            {
                if (stringToEncrypt == "") { return ""; }

                byte[] key = { }; // we are going to pass in the key portion in our method calls
                byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
                key = System.Text.Encoding.UTF8.GetBytes(EncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                // convert our input string to a byte array
                byte[] inputByteArray = System.Text.Encoding.UTF8.GetBytes(stringToEncrypt);

                //now encrypt the bytearray
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                // now return the byte array as a "safe for XMLDOM" Base64 String
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception e) { return e.Message; }
        }
    }
}