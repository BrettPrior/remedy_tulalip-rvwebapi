﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace RV.WebApi.Helpers
{
    public class EncryptionAES
    {

        public string EncryptionKey { get; private set; }

        public EncryptionAES()
        {
            EncryptionKey = "Rf!D RUL35";
        }

        public string EncryptAES(string stringToEncrypt)
        {
            if (stringToEncrypt == "")
            { return ""; }
            string encryptedString = string.Empty;
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            byte[] passwordBytes = Encoding.UTF8.GetBytes(EncryptionKey.Substring(0, 8));
            byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(stringToEncrypt);

            using (MemoryStream ms = new MemoryStream())
            using (RijndaelManaged Aes = new RijndaelManaged())
            {
                Aes.KeySize = 256;
                Aes.BlockSize = 128;
                var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                Aes.Key = key.GetBytes(Aes.KeySize / 8);
                Aes.IV = key.GetBytes(Aes.BlockSize / 8);
                Aes.Mode = CipherMode.CBC;

                using (var cs = new CryptoStream(ms, Aes.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                    cs.Close();
                }
                //Encoding encoding = Encoding.UTF8;
                encryptedString = Convert.ToBase64String(ms.ToArray());
            }


            return encryptedString;
        }

        public string DecryptAES(string stringToDecrypt)
        {
            if (stringToDecrypt == "")
            { return ""; }
            string decryptedString = string.Empty;
            byte[] bytesToBeDecrypted = Convert.FromBase64String(stringToDecrypt); //System.Text.Encoding.UTF8.GetBytes(stringToDecrypt)
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            byte[] passwordBytes = Encoding.UTF8.GetBytes(EncryptionKey.Substring(0, 8));

            using (MemoryStream ms = new MemoryStream())
            using (RijndaelManaged Aes = new RijndaelManaged())
            {
                Aes.KeySize = 256;
                Aes.BlockSize = 128;

                var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                Aes.Key = key.GetBytes(Aes.KeySize / 8);
                Aes.IV = key.GetBytes(Aes.BlockSize / 8);
                Aes.Mode = CipherMode.CBC;
                using (CryptoStream cs = new CryptoStream(ms, Aes.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                    cs.Close();
                }
                Encoding encoding = Encoding.UTF8;
                decryptedString = encoding.GetString(ms.ToArray());
            }
            return decryptedString;
        }

    }
}